require_relative 'requests'
require 'json'
include Requests

module API
  class Friend
    @id
    @first_name
    @last_name

    def initialize(id, first_name, last_name)
      @id = id
      @first_name = first_name
      @last_name = last_name
    end

    def self.get_friends(token)
      url = 'https://api.vk.com/method/friends.get'
      params = {'order' => 'hints', 'fields' => 'nickname', 'access_token' => token}
      request = PostRequest.new(url, params)
      response = request.request
      return parse_json response.body

    end


    def to_s
      "<td>#{@id}</td><td>#{@first_name}</td><td>#{@last_name}</td>"
    end

    private
    def self.parse_json(string)
      output = []
      json = JSON.parse(string)
      friends = json['response']
      friends.each { |friend| output << Friend.new(friend['uid'], friend['first_name'], friend['last_name']) } unless friends.nil?
      return output
    end
  end
end
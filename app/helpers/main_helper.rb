require_relative 'vk/auth'
require_relative 'vk/requests'
require_relative 'vk/vkapi'
include API
include Authentication
include Requests

module MainHelper
  def connect(email, password)
    auth = Auth.new
    token = auth.heroku_auth email, password
    friends = Friend.get_friends token
    output = "<tr><td colspan=3>#{token}</td></tr>"
    friends.each { |friend| output += "<tr>#{friend}</tr>"}
    output
  end

  def test_connection
    string_url = "https://www.apple.com/"
    request = GetRequest.new(string_url)
    response = request.request
    response.body
  end
end
